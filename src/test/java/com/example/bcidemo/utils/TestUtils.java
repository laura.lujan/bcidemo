package com.example.bcidemo.utils;

import com.example.bcidemo.dto.CreateUserRequestDto;
import com.example.bcidemo.dto.CreatedUserResponseDto;
import com.example.bcidemo.dto.UserLoginResponseDto;
import com.example.bcidemo.entity.UserEntity;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class TestUtils {
    private TestUtils() {
    }

    public static final UserEntity USER_ENTITY_STUB = UserEntity.builder()
            .name("Test")
            .email("test.tester@gmail.com")
            .password("2er5dfWio")
            .lastLogin(LocalDateTime.now())
            .createdDate(LocalDateTime.now())
            .isActive(true)
            .phones(new ArrayList<>())
            .build();

    public static final UserEntity USER_ENTITY_INVALID_MAIL_STUB = UserEntity.builder()
            .name("Test")
            .email("test.tester#gmail.com")
            .password("2er5dfWio")
            .lastLogin(LocalDateTime.now())
            .createdDate(LocalDateTime.now())
            .isActive(true)
            .phones(new ArrayList<>())
            .build();

    public static final CreatedUserResponseDto CREATED_USER_RESPONSE_DTO_STUB = CreatedUserResponseDto.builder()
            .id(1)
            .lastLogin(LocalDateTime.now())
            .createdDate(LocalDateTime.now())
            .token("eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTQxMTAxNDIsImF1ZCI6Im1vY2tlZEF1ZGllbmNlIiwiaXNzIjoiaHR0cHM6Ly9tb2NrZWQtaXNzdWVyLmNvbSIsImV4cCI6MTY5NDExMDE3MiwidXNlck5hbWUiOiJJbmVzIiwidXNlckVtYWlsIjoiaW5lcy5sdWphbkBnbWFpbC5jb20ifQ.ycbmbp8Qcj6h_D_G_1NZSKUWdAujW0fkcVLtsfTZiSE")
            .isActive(true)
            .build();

    public static final CreateUserRequestDto CREATE_USER_REQUEST_DTO_STUB = CreateUserRequestDto.builder()
            .name("Test")
            .email("test.tester@gmail.com")
            .password("2er5dfWio")
            .phones(new ArrayList<>())
            .build();

    public static final CreateUserRequestDto CREATE_USER_REQUEST_DTO_INVALID_MAIL_STUB = CreateUserRequestDto.builder()
            .name("Test")
            .email("test.tester$gmail.com")
            .password("2er5dfWio")
            .phones(new ArrayList<>())
            .build();

    public static final CreateUserRequestDto CREATE_USER_REQUEST_DTO_INVALID_PASS_STUB = CreateUserRequestDto.builder()
            .name("Test")
            .email("test.tester@gmail.com")
            .password("2e45dfWio")
            .phones(new ArrayList<>())
            .build();

    public static final CreateUserRequestDto CREATE_USER_REQUEST_DTO_PASS_REQUIRED_STUB = CreateUserRequestDto.builder()
            .name("Test")
            .email("test.tester@gmail.com")
            .phones(new ArrayList<>())
            .build();

    public static final CreateUserRequestDto CREATE_USER_REQUEST_DTO_MAIL_REQUIRED_STUB = CreateUserRequestDto.builder()
            .name("Test")
            .password("2e4dfWio")
            .phones(new ArrayList<>())
            .build();

    public static final UserLoginResponseDto USER_LOGIN_DTO_STUB = UserLoginResponseDto.builder()
            .id(1)
            .name("Test")
            .email("test.tester@gmail.com")
            .password("2er5dfWio")
            .lastLogin(LocalDateTime.now())
            .createdDate(LocalDateTime.now())
            .isActive(true)
            .token("eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTQxMTAxNDIsImF1ZCI6Im1vY2tlZEF1ZGllbmNlIiwiaXNzIjoiaHR0cHM6Ly9tb2NrZWQtaXNzdWVyLmNvbSIsImV4cCI6MTY5NDExMDE3MiwidXNlck5hbWUiOiJJbmVzIiwidXNlckVtYWlsIjoiaW5lcy5sdWphbkBnbWFpbC5jb20ifQ.ycbmbp8Qcj6h_D_G_1NZSKUWdAujW0fkcVLtsfTZiSE")
            .phones(new ArrayList<>())
            .build();

    public static final String TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTQxMTAxNDIsImF1ZCI6Im1vY2tlZEF1ZGllbmNlIiwiaXNzIjoiaHR0cHM6Ly9tb2NrZWQtaXNzdWVyLmNvbSIsImV4cCI6MTY5NDExMDE3MiwidXNlck5hbWUiOiJJbmVzIiwidXNlckVtYWlsIjoiaW5lcy5sdWphbkBnbWFpbC5jb20ifQ.ycbmbp8Qcj6h_D_G_1NZSKUWdAujW0fkcVLtsfTZiSE";

    public static final String PASSWORD_DECRYPTED = "2er5dfWio";
    public static final String PASSWORD_ENCRYPTED = "7UQatG7bGVbuF85/JxMPfA==";

}
