package com.example.bcidemo.policy;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class EmailValidationPolicyTest {
    @Test
    @DisplayName("Validate emailAddress successfully")
    public void testValidEmail() {
        String emailAddress = "JohonDoe@domain.com";
        assertTrue(EmailValidationPolicy.emailPatternMatches(emailAddress));
    }

    @Test
    @DisplayName("Domain EmailAddress error")
    public void testInvalidEmailDomain() {
        String emailAddress = "JohonDoe@.com";
        assertFalse(EmailValidationPolicy.emailPatternMatches(emailAddress));
    }

    @Test
    @DisplayName("Name EmailAddress error")
    public void testInvalidEmailName() {
        String emailAddress = "Johon...Doe@domain.com";
        assertFalse(EmailValidationPolicy.emailPatternMatches(emailAddress));
    }
}