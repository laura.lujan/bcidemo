package com.example.bcidemo.policy;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class PasswordValiationPolicyTest {

    @Test
    @DisplayName("Validate password successfully")
    public void testValidPassword() {
        String password = "as3df4Mjs";
        assertTrue(PasswordValiationPolicy.passwordPatternMatches(password));
    }

    @Test
    @DisplayName("Validate long password fail")
    public void testValidLongPassword() {
        String password = "as3df4M";
        assertFalse(PasswordValiationPolicy.passwordPatternMatches(password));
    }

    @Test
    @DisplayName("Validate password without upperCase letter")
    public void testValidIncorrectPassword() {
        String password = "as3df4jsdfgg";
        assertFalse(PasswordValiationPolicy.passwordPatternMatches(password));
    }

}