package com.example.bcidemo.service.impl;

import com.example.bcidemo.exeption.UserNotFoundException;
import com.example.bcidemo.exeption.ValidationException;
import com.example.bcidemo.mapper.UserMapper;
import com.example.bcidemo.repository.UserRepository;
import com.example.bcidemo.utils.EncryptDecrypt;
import com.example.bcidemo.utils.JwtToken;
import com.example.bcidemo.utils.TestUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {
    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private JwtToken jwtToken;

    @Mock
    private EncryptDecrypt encryptDecrypt;
    @Mock
    private UserMapper userMapper;

    @Test
    @DisplayName("User created Ok")
    public void createUserOk(){

        Mockito.when(userRepository.findByEmail(any())).thenReturn(Optional.empty());
        Mockito.when(userRepository.save(any())).thenReturn(TestUtils.USER_ENTITY_STUB);
        Mockito.when(jwtToken.getToken(TestUtils.USER_ENTITY_STUB.getName(),
                        TestUtils.USER_ENTITY_STUB.getEmail())).thenReturn(TestUtils.TOKEN);
        Mockito.when(encryptDecrypt.encrypt(TestUtils.USER_ENTITY_STUB.getPassword()))
                        .thenReturn(TestUtils.PASSWORD_ENCRYPTED);
        Assertions.assertDoesNotThrow(() -> userService.persistUser(TestUtils.CREATE_USER_REQUEST_DTO_STUB));
        verify(userRepository, times(1)).save(any());
        verify(userRepository, times(1)).findByEmail(any());
    }

    @Test
    @DisplayName("Create user Already exists")
    public void createUserAlreadyExist(){
        Mockito.when(userRepository.findByEmail(any())).thenReturn(Optional.of(TestUtils.USER_ENTITY_STUB));
        Throwable exception = assertThrows(ValidationException.class, () ->
                userService.persistUser(TestUtils.CREATE_USER_REQUEST_DTO_STUB));
        assertEquals("User already exist",exception.getMessage());

    }

    @Test
    @DisplayName("Create user invalid mail")
    public void createUserInvalidMail(){
        Mockito.when(userRepository.findByEmail(any())).thenReturn(Optional.empty());
        Throwable exception = assertThrows(ValidationException.class, () ->
                userService.persistUser(TestUtils.CREATE_USER_REQUEST_DTO_INVALID_MAIL_STUB));
        assertEquals("User email is invalid",exception.getMessage());

    }

    @Test
    @DisplayName("Create user invalid password")
    public void createUserInvalidPassword(){
        Mockito.when(userRepository.findByEmail(any())).thenReturn(Optional.empty());
        Throwable exception = assertThrows(ValidationException.class, () ->
                userService.persistUser(TestUtils.CREATE_USER_REQUEST_DTO_INVALID_PASS_STUB));
        assertEquals("User password is invalid",exception.getMessage());

    }

    @Test
    @DisplayName("Create user mail required")
    public void createUserMailRequired(){
        Mockito.when(userRepository.findByEmail(any())).thenReturn(Optional.empty());
        Throwable exception = assertThrows(ValidationException.class, () ->
                userService.persistUser(TestUtils.CREATE_USER_REQUEST_DTO_MAIL_REQUIRED_STUB));
        assertEquals("Email is required",exception.getMessage());

    }

    @Test
    @DisplayName("Create user password required")
    public void createUserPasswordRequired(){
        Mockito.when(userRepository.findByEmail(any())).thenReturn(Optional.empty());
        Throwable exception = assertThrows(ValidationException.class, () ->
                userService.persistUser(TestUtils.CREATE_USER_REQUEST_DTO_PASS_REQUIRED_STUB));
        assertEquals("Password is required",exception.getMessage());

    }

    @Test
    @DisplayName("Login User Ok")
    public void loginUserOk(){

        Mockito.when(userRepository.findByEmail(any())).thenReturn(Optional.of(TestUtils.USER_ENTITY_STUB));
        Mockito.when(userRepository.save(any())).thenReturn(TestUtils.USER_ENTITY_STUB);
        Mockito.when(encryptDecrypt.decrypt(any()))
                .thenReturn(TestUtils.PASSWORD_DECRYPTED);
        Assertions.assertDoesNotThrow(() -> userService.getUser(TestUtils.TOKEN));

        verify(userRepository, times(1)).save(any());
        verify(userRepository, times(1)).findByEmail(any());
    }

    @Test
    @DisplayName("Login user does not exists")
    public void loginUserNotExist(){
        Mockito.when(userRepository.findByEmail(any())).thenReturn(Optional.empty());
        Throwable exception = assertThrows(UserNotFoundException.class, () ->
                userService.getUser(TestUtils.TOKEN));
        assertEquals("User does not exists",exception.getMessage());

    }

}