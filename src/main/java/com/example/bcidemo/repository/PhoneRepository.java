package com.example.bcidemo.repository;

import com.example.bcidemo.entity.PhoneEntity;
import org.springframework.data.repository.CrudRepository;

public interface PhoneRepository extends CrudRepository<PhoneEntity, Integer> {
}
