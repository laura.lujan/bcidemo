package com.example.bcidemo.policy;

import java.util.regex.Pattern;

public class EmailValidationPolicy {

    private static final String emailRegexPattern
            = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";

    public static boolean emailPatternMatches(String emailAddress) {
        return Pattern.compile(emailRegexPattern)
                .matcher(emailAddress)
                .matches();
    }
}
