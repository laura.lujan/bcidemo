package com.example.bcidemo.policy;

import java.util.regex.Pattern;

public class PasswordValiationPolicy {
    private static final String passwordRegexPattern = "^(?=(?:[^A-Z]*[A-Z]){1}[^A-Z]*$)(?=(?:[^0-9]*[0-9]){2}[^0-9]*$)[A-Za-z0-9]{8,12}$";

    public static boolean passwordPatternMatches(String emailAddress) {
        return Pattern.compile(passwordRegexPattern)
                .matcher(emailAddress)
                .matches();
    }
}