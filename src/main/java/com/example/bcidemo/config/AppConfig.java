package com.example.bcidemo.config;

import com.example.bcidemo.repository.UserRepository;
import com.example.bcidemo.service.UserService;
import com.example.bcidemo.service.impl.UserServiceImpl;
import com.example.bcidemo.utils.EncryptDecrypt;
import com.example.bcidemo.utils.JwtToken;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    public UserService userService(final UserRepository userRepository,
                                   final JwtToken jwtToken,
                                   final EncryptDecrypt encryptDecrypt) {
        return (UserService) new UserServiceImpl(userRepository, jwtToken, encryptDecrypt);
    }

}
