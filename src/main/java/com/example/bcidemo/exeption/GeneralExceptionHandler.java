package com.example.bcidemo.exeption;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GeneralExceptionHandler {

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<Error> methodValidationException(ValidationException ex) {
        Error errorDetail = Error.builder()
                .timestamp(LocalDateTime.now())
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .detail(ex.getMessage()).build();
        return new ResponseEntity<>(errorDetail, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<Error> methodUserNotFoundException(UserNotFoundException ex) {
        Error errorDetail = Error.builder()
                .timestamp(LocalDateTime.now())
                .statusCode(HttpStatus.NOT_FOUND.value())
                .detail(ex.getMessage()).build();
        return new ResponseEntity<>(errorDetail, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Error> methodException(Exception ex) {
        Error errorDetail = Error.builder()
                .timestamp(LocalDateTime.now())
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .detail(ex.getMessage()).build();
        return new ResponseEntity<>(errorDetail, HttpStatus.BAD_REQUEST);
    }
}
