package com.example.bcidemo.exeption;

import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Error {
    private LocalDateTime timestamp;
    private int statusCode;
    private String detail;
}
