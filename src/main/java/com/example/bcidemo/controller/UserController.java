package com.example.bcidemo.controller;

import com.example.bcidemo.dto.CreateUserRequestDto;
import com.example.bcidemo.dto.CreatedUserResponseDto;
import com.example.bcidemo.dto.UserLoginResponseDto;
import com.example.bcidemo.service.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/v1/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/sign-up")
    public ResponseEntity singUp(@RequestBody final CreateUserRequestDto createUserRequestDto) {
        CreatedUserResponseDto createdUserResponseDto = userService.persistUser(createUserRequestDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdUserResponseDto);
    }

    @GetMapping("/login")
    public ResponseEntity login(@RequestHeader(HttpHeaders.AUTHORIZATION) String token) {
        UserLoginResponseDto userLoginResponseDto = userService.getUser(token);

        return ResponseEntity.status(HttpStatus.CREATED).body(userLoginResponseDto);
    }
}
