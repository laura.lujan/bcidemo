package com.example.bcidemo.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreatedUserResponseDto {
    private Integer id;
    private LocalDateTime createdDate;
    private LocalDateTime lastLogin;
    private String token;
    private boolean isActive;
}
