package com.example.bcidemo.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PhoneDto {
    private long number;
    private int cityCode;
    private String countryCode;
}
