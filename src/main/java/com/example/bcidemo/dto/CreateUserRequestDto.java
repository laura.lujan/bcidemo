package com.example.bcidemo.dto;

import java.util.List;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateUserRequestDto {
    private String name;
    private String email;
    private String password;
    private List<PhoneDto> phones;

}
