package com.example.bcidemo.mapper;

import com.example.bcidemo.dto.CreateUserRequestDto;
import com.example.bcidemo.dto.CreatedUserResponseDto;
import com.example.bcidemo.dto.PhoneDto;
import com.example.bcidemo.dto.UserLoginResponseDto;
import com.example.bcidemo.entity.PhoneEntity;
import com.example.bcidemo.entity.UserEntity;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;
@NoArgsConstructor
public class UserMapper {

    public static UserEntity createUserRequestDtoToUserEntity (CreateUserRequestDto userRequestDto){

        List<PhoneEntity> phoneEntityList = userRequestDto.getPhones()
                .stream()
                .map(phoneDto -> {
                    return new PhoneMapper().phoneDtoToPhoneEntity(phoneDto);
                }).collect(Collectors.toList());

        return UserEntity.builder()
                .name(userRequestDto.getName())
                .email(userRequestDto.getEmail())
                .password(userRequestDto.getPassword())
                .phones(phoneEntityList)
                .build();
    }


    public static CreatedUserResponseDto userEntityToCreateUserResponseDto (UserEntity userEntity){

        return CreatedUserResponseDto.builder()
                .createdDate(userEntity.getCreatedDate())
                .id(userEntity.getUserid())
                .lastLogin(userEntity.getLastLogin())
                .isActive(userEntity.isActive())
                .build();
    }

    public static UserLoginResponseDto userEntityToUserLoginResponseDto (UserEntity userEntity){

        List<PhoneDto> phoneDtoList = userEntity.getPhones()
                .stream()
                .map(phoneEntity -> {
                    return new PhoneMapper().phoneEntityToPhoneDto(phoneEntity);
                }).collect(Collectors.toList());

        return UserLoginResponseDto.builder()
                .id(userEntity.getUserid())
                .createdDate(userEntity.getCreatedDate())
                .lastLogin(userEntity.getLastLogin())
                .isActive(userEntity.isActive())
                .name(userEntity.getName())
                .email(userEntity.getEmail())
                .password(userEntity.getPassword())
                .phones(phoneDtoList)
                .build();
    }
}
