package com.example.bcidemo.mapper;

import com.example.bcidemo.dto.PhoneDto;
import com.example.bcidemo.entity.PhoneEntity;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class PhoneMapper {

    public PhoneDto phoneEntityToPhoneDto(PhoneEntity phoneEntity){
        return PhoneDto.builder()
                .number(phoneEntity.getNumber())
                .cityCode(phoneEntity.getCitycode())
                .countryCode(phoneEntity.getCountrycode())
                .build();
    }

    public  PhoneEntity phoneDtoToPhoneEntity (PhoneDto phoneDto){
        return PhoneEntity.builder()
                .number(phoneDto.getNumber())
                .citycode(phoneDto.getCityCode())
                .countrycode(phoneDto.getCountryCode())
                .build();
    }
}
