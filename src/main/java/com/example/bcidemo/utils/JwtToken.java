package com.example.bcidemo.utils;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Base64;
import java.util.Date;

@Component
public class JwtToken {

    private final String jwtIssuer;
    private final String jwtSecret;
    private final String jwtAudience;
    private final int jwtValidity;

    public JwtToken(@Value("${jwtIssuer}") final String jwtIssuer,
                    @Value("${jwtSecret}") final String jwtSecret,
                    @Value("${jwtAudience}") final String jwtAudience,
                    @Value("${jwtValidity}") final int jwtValidity
                    ) {
        this.jwtIssuer = jwtIssuer;
        this.jwtSecret = jwtSecret;
        this.jwtAudience = jwtAudience;
        this.jwtValidity = jwtValidity;
    }

    public String getToken(String userName, String userEmail) {
        final LocalDateTime ldNow = LocalDateTime.now();
        //long validity = 30000;
        final LocalDateTime ldExpiration = ldNow.plus(Duration.ofMillis(jwtValidity));
        final Date now = Date.from(ldNow.atZone(ZoneId.systemDefault()).toInstant());
        final Date expirationDate = Date.from(ldExpiration.atZone(ZoneId.systemDefault()).toInstant());


        return Jwts.builder()
                .setIssuedAt(now)
                .setAudience(jwtAudience)
                .setIssuer(this.jwtIssuer)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS256, jwtSecret.getBytes(StandardCharsets.UTF_8))
                .claim("userName", userName)
                .claim("userEmail", userEmail)
                .compact();
    }

    public String getEmailFromToken(String token) throws ParseException {
        Base64.Decoder decoder = Base64.getUrlDecoder();
        String[] split_string = token.split("\\.");
        String header = split_string[0];
        String payload = split_string[1];
        String body = new String(decoder.decode(payload));
        JSONParser jsonObject = new JSONParser();
        Object jsonObj = jsonObject.parse(body);
        JSONObject json = (JSONObject) jsonObj;

        return (String)json.get("userEmail");
    }
}
