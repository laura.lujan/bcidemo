package com.example.bcidemo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Entity
@Table(name="phone")
public class PhoneEntity {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer phoneid;

    @ManyToOne
    @JoinColumn(name="userid", insertable = false, updatable = false)
    private UserEntity user;

    private long number;
    private int citycode;
    private String countrycode;
}
