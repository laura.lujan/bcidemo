package com.example.bcidemo.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Entity
@Table(name="user")
public class UserEntity {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer userid;
    private String name;
    private String email;
    private String password;
    private LocalDateTime createdDate;
    private LocalDateTime lastLogin;

    private boolean isActive = true;

    @OneToMany(mappedBy = "user")
    private List<PhoneEntity> phones;
}
