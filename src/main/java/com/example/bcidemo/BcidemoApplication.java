package com.example.bcidemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.example.bcidemo")
public class BcidemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BcidemoApplication.class, args);
	}

}
