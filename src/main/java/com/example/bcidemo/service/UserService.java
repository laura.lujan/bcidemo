package com.example.bcidemo.service;

import com.example.bcidemo.dto.CreateUserRequestDto;
import com.example.bcidemo.dto.CreatedUserResponseDto;
import com.example.bcidemo.dto.UserLoginResponseDto;
import org.springframework.stereotype.Component;

@Component
public interface UserService {
    CreatedUserResponseDto persistUser(CreateUserRequestDto createUserRequestDto);
    UserLoginResponseDto getUser(String token);
}
