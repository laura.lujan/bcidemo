package com.example.bcidemo.service.impl;

import static com.example.bcidemo.mapper.UserMapper.*;
import static com.example.bcidemo.policy.EmailValidationPolicy.emailPatternMatches;
import static com.example.bcidemo.policy.PasswordValiationPolicy.passwordPatternMatches;

import com.example.bcidemo.dto.CreateUserRequestDto;
import com.example.bcidemo.dto.CreatedUserResponseDto;
import com.example.bcidemo.dto.UserLoginResponseDto;
import com.example.bcidemo.entity.UserEntity;
import com.example.bcidemo.exeption.UserNotFoundException;
import com.example.bcidemo.exeption.ValidationException;
import com.example.bcidemo.repository.UserRepository;
import com.example.bcidemo.service.UserService;
import com.example.bcidemo.utils.EncryptDecrypt;
import com.example.bcidemo.utils.JwtToken;
import org.json.simple.parser.ParseException;

import java.time.LocalDateTime;
import java.util.Optional;

public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final JwtToken jwtToken;

    private final EncryptDecrypt encryptDecrypt;


    public UserServiceImpl(UserRepository userRepository,
                           JwtToken jwtToken,
                           EncryptDecrypt encryptDecrypt) {
        this.userRepository = userRepository;
        this.jwtToken = jwtToken;
        this.encryptDecrypt = encryptDecrypt;
    }

    public CreatedUserResponseDto persistUser(CreateUserRequestDto createUserRequestDto) {

        UserEntity userEntity = createUserRequestDtoToUserEntity(createUserRequestDto);
        userEntity.setActive(true);
        userEntity.setCreatedDate(LocalDateTime.now());
        Optional<UserEntity> optionalUserEntity = userRepository.findByEmail(createUserRequestDto.getEmail());
        if (optionalUserEntity.isPresent())
            throw new ValidationException("User already exist");
        validateUser(userEntity);
        userEntity.setPassword(encryptDecrypt.encrypt(userEntity.getPassword()));

        UserEntity userEntitySaved = userRepository.save(userEntity);

        CreatedUserResponseDto userResponseDto = userEntityToCreateUserResponseDto(userEntitySaved);
        userResponseDto.setToken(jwtToken.getToken(userEntitySaved.getName(),userEntitySaved.getEmail()));
        return userResponseDto;
    }

    public UserLoginResponseDto getUser(String token) {
        String email="";
        try {
            email = jwtToken.getEmailFromToken(token);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        Optional<UserEntity> optionalUserEntity = userRepository.findByEmail(email);
        if(!optionalUserEntity.isPresent())
            throw new UserNotFoundException("User does not exists");
        optionalUserEntity.get().setLastLogin(LocalDateTime.now());
        UserEntity optionalUserEntityLoginSave = userRepository.save(optionalUserEntity.get());
        UserLoginResponseDto userLoginResponseDto = userEntityToUserLoginResponseDto(optionalUserEntityLoginSave);
        userLoginResponseDto.setToken(jwtToken.getToken(userLoginResponseDto.getName(), userLoginResponseDto.getEmail()));
        userLoginResponseDto.setPassword(encryptDecrypt.decrypt(userLoginResponseDto.getPassword()));
        return userLoginResponseDto;
    }

    public static void validateUser (UserEntity userEntity){
        if (userEntity.getPassword() == null)
            throw new ValidationException("Password is required");
        if (userEntity.getEmail() == null)
            throw new ValidationException("Email is required");
        if (!emailPatternMatches(userEntity.getEmail()))
            throw new ValidationException("User email is invalid");
        if (!passwordPatternMatches(userEntity.getPassword()))
            throw new ValidationException("User password is invalid");
    }
}
