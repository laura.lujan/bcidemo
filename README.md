# bcidemo


## Construccion
El proyecto esta construido utilizando:
- SpringBoot
- Gradle
- Java 8
- Lombok
- BD_ H2

Para testing:
- JUnit
- Mockito

## Caracteristicas propias de Java8 utilizadas:

- Optional
- Streams
- Lambda
- LocalDateTime

## Para correr el proyecto:

1. Desde la carpeta raiz ejecutar: **gradle clean build**
1. Desde la carpeta donde se genero el jar (capeta raiz\build\libs) ejecutar: **java -jar bcidemo-0.0.1-SNAPSHOT.jar**

## Detalle de los endpoints:

> Swagger: **http://localhost:8080/swagger-ui/index.html**

## SignUp
    
- **URI**: /v1/users/sing-up
- **Metodo**: POST

 Datos No requeridos en el Request: name y phones
        

    Request Body:
        {
        "name": "xxx",
        "email": "xxx.xxxx@xxxxx.com",
        "password": "12eAvftgg",
        "phones": [
            {
                "number": 7589654,
                "citycode": 221,
                "contrycode": "54"
            }
        ]
         }

    Response:
        {
            "id": 1,
            "createdDate": "2023-09-07T15:08:58.7872248",
            "lastLogin": null,
            "token": "",
            "active": true
        }

## Login
    
- **URI**: /v1/users/login
- **Metodo**: GET

Requiere pasar en Authentication del Header un Bearer token con una Claim llamada userEmail con el email del usuario que se va a loguear.


    Response:
        {
            "id": 1,
            "createdDate": "2023-09-10T09:51:48.438754",
            "lastLogin": "2023-09-10T09:52:43.6052752",
            "token": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
            "name": "Ines",
            "email": "ines.lujan@gmail.com",
            "password": "xxxxxx",
            "phones": [],
            "active": true
    }

